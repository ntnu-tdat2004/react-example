// @flow
import * as React from 'react';
import {Component} from 'react-simplified';
import ReactDOM from 'react-dom';
import {HashRouter, Route} from 'react-router-dom';
import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();
import {Article, articleService} from './services';
import {Alert, NavigationBar, Card, Table, Form} from './widgets';

class Home extends Component {
  render() {
    return <Card title="Example App">Demonstration of React with Flow and server communication</Card>;
  }
}

class ArticleDetails extends Component<{match: {params: {id: number}}}> {
  article: ?Article = null;

  render() {
    if (!this.article) return null;
    return (
      <Card title={'Article: ' + this.article.title}>
        <div>
          <div>
            <strong>{this.article.abstract}</strong>
          </div>
          <div>{this.article.text}</div>
        </div>
      </Card>
    );
  }

  mounted() {
    articleService
      .getArticle(this.props.match.params.id)
      .then(article => (this.article = article))
      .catch((error: Error) =>
        Alert.danger('Error getting article ' + this.props.match.params.id + ': ' + error.message)
      );
  }
}

class NewArticle extends Component<{onAdd: () => void}> {
  form;
  title;
  abstract;
  text;

  render() {
    return (
      <Card title="New Article">
        <Form
          ref={e => (this.form = e)}
          onSubmit={this.addArticle}
          submitLabel="Add Article"
          groups={[
            {label: 'Title', input: <input ref={e => (this.title = e)} type="text" required />},
            {label: 'Abstract', input: <textarea ref={e => (this.abstract = e)} rows="2" required />},
            {label: 'Text', input: <textarea ref={e => (this.text = e)} rows="3" required />},
            {
              checkInputs: [
                {
                  label: 'I have read, understand and accept the terms and ...',
                  input: <input type="checkbox" required />
                }
              ]
            }
          ]}
        />
      </Card>
    );
  }

  addArticle() {
    if (!this.title || !this.abstract || !this.text) return;
    articleService
      .addArticle(this.title.value, this.abstract.value, this.text.value)
      .then(id => {
        if (this.form) this.form.reset();
        if (this.props.onAdd) this.props.onAdd();
        history.push('/articles/' + id);
      })
      .catch((error: Error) => Alert.danger('Error adding article: ' + error.message));
  }
}

class Articles extends Component {
  table;
  newArticle;

  render() {
    return (
      <div>
        <Card title="Articles">
          <Table ref={e => (this.table = e)} header={['Title', 'Abstract']} onRowClick={this.selectArticle} />
        </Card>
        <Route exact path="/articles/:id" component={ArticleDetails} />
        <NewArticle ref={e => (this.newArticle = e)} onAdd={this.update} />
      </div>
    );
  }

  // Helper function to update component
  update() {
    articleService
      .getArticles()
      .then(articles => {
        if (this.table)
          this.table.setRows(articles.map(article => ({id: article.id, cells: [article.title, article.abstract]})));
      })
      .catch((error: Error) => Alert.danger('Error getting articles: ' + error.message));
  }

  mounted() {
    this.update();
  }

  selectArticle(id) {
    history.push('/articles/' + id);
  }
}

let root = document.getElementById('root');
if (root) {
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <NavigationBar brand="React Example" links={[{to: '/articles', text: 'Articles'}]} />
        <Route exact path="/" component={Home} />
        <Route path="/articles" component={Articles} />
      </div>
    </HashRouter>,
    root
  );
}
