// @flow
import axios from 'axios';
axios.interceptors.response.use(response => response.data);

export class Article {
  id: number;
  title: string;
  abstract: string;
  text: string;
}

class ArticleService {
  getArticles(): Promise<Article[]> {
    return axios.get('/articles');
  }

  getArticle(id: number): Promise<Article> {
    return axios.get('/articles/' + id);
  }

  addArticle(title: string, abstract: string, text: string): Promise<number> {
    return axios.post('/articles', {title: title, abstract: abstract, text: text});
  }
}
export let articleService = new ArticleService();
